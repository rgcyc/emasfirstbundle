Pod::Spec.new do |s|

  s.name         = "EMASFirstBundle"
  #s.version      = "1.0.0"
  s.version      = "${version}"
  s.summary      = "EMASFirstBundle类库"

  s.description  = <<-DESC
                   * Detail about EMASFirstBundle framework.
                   DESC

  s.homepage     = "http://10.125.60.155/zhishui.lcq/EMASFirstBundle/tree/master"
  s.license = {
  :type => 'Copyright',
  :text => <<-LICENSE
       Alibaba-INC copyright
  LICENSE
  }
  s.author       = { "执水" => "zhishui.lcq@alibaba-inc.com" }
  s.platform     = :ios
  s.ios.deployment_target = '8.0'
  #s.source = { :git => "git@10.125.60.155:zhishui.lcq/EMASFirstBundle.git", :tag => s.version }
  s.source       = { :http => '${url}'}
  #s.preserve_paths = "EMASFirstBundle.framework/*"
  #s.resources  = "EMASFirstBundle.framework/*.{bundle,xcassets}"
  s.vendored_frameworks = 'EMASFirstBundle.framework'
  s.requires_arc = true
  s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '$(PODS_ROOT)/EMASFirstBundle' }

  #s.dependency 'XXXX'

end
