//
//  FirstBundleInterface.m
//  EMASFirstBundle
//
//  Created by zhishui.lcq on 2018/1/3.
//  Copyright © 2018年 Alipay. All rights reserved.
//

#import "FirstBundleInterface.h"

@implementation FirstBundleInterface

+ (BOOL)setAppVersion:(NSString *)version
{
    NSLog(@"version = %@", version);
    return YES;
}

@end
