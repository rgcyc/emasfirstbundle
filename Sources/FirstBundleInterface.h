//
//  FirstBundleInterface.h
//  EMASFirstBundle
//
//  Created by zhishui.lcq on 2018/1/3.
//  Copyright © 2018年 Alipay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FirstBundleInterface : NSObject

+ (BOOL)setAppVersion:(NSString *)version;

@end
